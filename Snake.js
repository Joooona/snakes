//Perinteinen matopeli

/*
Sis�lt�� j�rjestyksess� seuraavat toiminnot:
-Pelin alustus ja muuttujien asetus
-K��rmeen liikkeet ja kontrollointi
-T�rm�yksien hallinta
-Sy�mistoiminnot
-Grafiikka
-Sama peli tekstimuotoisena
-Pelin runko

Lis�ysehdotukset
-Ruoka ei satunnaistulostu k��rmeen sis�lle
*/

//---Pelin alustus ja muuttujien asetus-----------------

//Lyhenn�kset ja yhteydet HTML-tiedostoon
$(document).ready(function(){
var c = document.getElementById("snake");
var p = document.getElementById("pisteita");
var vie = document.getElementById("viestit");
var ctx = c.getContext("2d");
var w = c.offsetWidth;
var h = c.offsetHeight;

//K�ytt�j�n vapaasti muokattavissa olevat arvot: k��rmeen pituus ja nopeus alussa.
//***********************
var length= 2;
var speed= 100;
//***********************

//K��rmeen suunta
var direction="r";

//Pelin alustusehto
var m = "";

//Ruoan satunnaisgenerointi
var food = [(Math.floor((Math.random() * 10)))*(w/10) , (Math.floor((Math.random() * 10)))*(h/10)];

//Pisteenlaskun muuttujat
var piste = 0;
var pisteet = [];
pisteet.push({x: 0, y: 0});
var str="";

//Yrityskertojen lukum��r�
var game = 0;

//K��rmen alustus kent�lle. Esim. t�ss� vasempaan yl�kulmaan.
var snake_array=[];
function snake_birth(){
for(i=length;i>=0;i--){
snake_array.push({x: i*10,y: 0});
}
}
snake_birth();

//Pisteiden alustus
function score(){
pisteet[game].x = piste;
for(i=0;i<=game;i++){
str = str + ", " + JSON.stringify(pisteet[i].x);
}
p.innerHTML= str;
}
score();

//Hyv� tulos
function goodscore(){
return (piste == 50);
}

//Hyv��n tulokseen liittyv� viesti
function score_declaration(){
if (goodscore()){
vie.innerHTML = "Hyv� tulos!";
setTimeout(function(){ vie.innerHTML = ""; },500);
setTimeout(function(){ vie.innerHTML = "Hyv� tulos!"; },1000);
setTimeout(function(){ vie.innerHTML = ""; },1500);
setTimeout(function(){ vie.innerHTML = "Hyv� tulos!"; },2000);
setTimeout(function(){ vie.innerHTML = ""; },2500);
}
}

//Uuden pelin alustus
function initial(){

//Pelitekniikka
score_declaration();
game = game + 1;
m="";

//K��rme
snake_array = [];
snake_birth();
direction = "r";

//Pisteenlasku
str ="";
pisteet.push({x: 0, y: 0});
piste = 0;
score();
}

//---K��rmeen liikkeet ja kontrollointi----------------------------

//K��rmeen kontrollointi
$(document).keydown(function(e){
var key = e.which;
if(key == "37" && direction != "r"){ direction = "l";}
else if (key == "38" && direction != "d"){ direction = "u";}
else if (key == "39" && direction != "l"){ direction = "r";}
else if (key == "40" && direction != "u"){ direction = "d";}
})

//K��rmeen liike
function snake_move(){
if(direction == "r"){
snake_array.unshift({x: (snake_array[0].x + 10), y: (snake_array[0].y)});
snake_array.pop();
}
else if(direction == "l"){
snake_array.unshift({x: (snake_array[0].x - 10), y: (snake_array[0].y)});
snake_array.pop();
}
else if(direction == "u"){
snake_array.unshift({x: (snake_array[0].x), y: (snake_array[0].y - 10 )});
snake_array.pop();
}
else if(direction == "d"){
snake_array.unshift({x: (snake_array[0].x), y: (snake_array[0].y + 10)});
snake_array.pop();
}
}

//---T�rm�yksien hallinta-------------------------------

//T�rm�yksien ehdot
function selfcollision(n){
return (snake_array[0].x == snake_array[n].x) && (snake_array[0].y == snake_array[n].y);
}

function bordercollision(){
return (snake_array[0].x == Math.floor(w) || snake_array[0].y == Math.floor(h))|| (snake_array[0].x == -10 || snake_array[0].y == -10);
}

//T�rm�ysprosessin hoito
function selfcollision_handler(){
for(n=snake_array.length-2;n>=1;n--){
//Vastaisuudelle huomio: miksi t�ss� ei toiminut, ett� toteutettavaan olisi tullut snake_arraysta riippuva fkt.
if (selfcollision(n)) { 
m="e"; 
}
}
if(m=="e"){
initial();
str="";
}
}

function bordercollision_handler(){
if(bordercollision()){ 
initial(); 
str = "";
}
}

//---Sy�mistoiminnot-----------------------------

//K��rmeen sy�miskasvu
function snake_eat(){
if(direction == "r"){
snake_array.unshift({x: (snake_array[0].x + 10), y: (snake_array[0].y)});
}
else if(direction == "l"){
snake_array.unshift({x: (snake_array[0].x - 10), y: (snake_array[0].y)});
}
else if(direction == "u"){
snake_array.unshift({x: (snake_array[0].x), y: (snake_array[0].y - 10 )});
}
else if(direction == "d"){
snake_array.unshift({x: (snake_array[0].x), y: (snake_array[0].y + 10)});
}
}

//Ruoan luominen kent�lle
function foodnew(){
food= [];
food = [(Math.floor((Math.random() * 10)))*(w/10) , (Math.floor((Math.random() * 10)))*(h/10)];
}

//Ruoan kohtaamisehto
function meet(){
return ((snake_array[0].x == food[0]) && (snake_array[0].y == food[1]))
}

//Sy�misprosessi
function eat(){

//K��rme
if(meet()){
foodnew();
snake_eat();

//Pisteet
piste = piste + 1;
str="";
score();
}
}

//---Grafiikka------------------------------

//Kentt�
function paint(){
ctx.fillStyle = "white";
ctx.fillRect(0,0,w,h);
ctx.strokeStyle ="black";
ctx.strokeRect(0,0,w,h);

//K��rme
for(i=snake_array.length-1;i>=0;i--){
ctx.strokeRect(snake_array[i].x,snake_array[i].y,10,10);
}

//Ruoka
ctx.strokeRect(food[0],food[1],10,10);
}

//---Peli tekstimuotoisena---------------------------------//Objektin arvo kirjallisena, testaukseen, muuta arvo my�s Prosessi-funktiosta.
/*
function show(){
var str1 = JSON.stringify(snake_array);
var str2 = JSON.stringify(food);
var str3 = JSON.stringify(meet());
p.innerHTML=str1 + str2 + str3 + length;
}
*/

//---Pelin runko------------------------------

//Prosessi
function process(){
snake_move();
eat();
selfcollision_handler();
bordercollision_handler();
paint();
/* //N�yt� t�m�, jos haluat tekstik�ytt�liittym�n esiin
show();
*/
start();
}

//Animointi
function start(){
var go = setTimeout(process,speed)
}

//Pelin k�ynnistys
start();

})